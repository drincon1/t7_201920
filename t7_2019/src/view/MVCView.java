package view;

import model.data_structures.GrafoNoDirigido;
import model.logic.Interseccion;
import model.logic.MVCModelo;

public class MVCView 
{
	//---------------------------------------------
	// Atributos
	//---------------------------------------------
	private MVCModelo modelo;



	//---------------------------------------------
	// Constructor 
	//---------------------------------------------
	public MVCView() 
	{
		modelo = new MVCModelo();
	}
	
	private void printMensaje(String mensaje)
	{
		System.out.println(mensaje);
	}
	
	public void printCarga()
	{
		printMensaje("Número de vertices: " + modelo.grafo.V);
		printMensaje("Número de arcos: " + modelo.grafo.E);
	}
	
	public void printCC(int cc)
	{
		printMensaje("Número de componentes conectadas: " + cc);
	}
	
	public void printGrafoJSON(GrafoNoDirigido<Integer, Interseccion> grafo)
	{
		printMensaje("**********Información grafo JSON**********");
		printMensaje("Número de vertices: " + grafo.V);
		printMensaje("Número de arcos: " + grafo.E);
	}

}
