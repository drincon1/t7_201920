package model.logic;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Iterator;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import model.data_structures.Arco;
import model.data_structures.GrafoNoDirigido;
import model.data_structures.Nodo;

public class MVCModelo 
{
	//---------------------------------------
	// Atributos
	//---------------------------------------

	/**
	 * Grafo de la malla vial de Bogota
	 */
	public GrafoNoDirigido<Integer, Interseccion> grafo;
	
	/**
	 * Número de componentes conectadas
	 */
	private int cc;
	
	
	/**
	 * Clase que carga el mapa
	 */
	public Maps mapa;
	
	//---------------------------------------------
	// Constructor 
	//---------------------------------------------
	/**
	 * Método constructor de la clase MVCModelo
	 */
	public MVCModelo() 
	{
		grafo = new GrafoNoDirigido<Integer, Interseccion>(228045);
	}

	//---------------------------------------
	// Métodos
	//---------------------------------------

	/**
	 * Método que carga los archivos:
	 * bogota_vertices.txt
	 * bogota_arcos.txt
	 * @throws IOException 
	 */
	public void cargar() throws IOException
	{

		try 
		{
			BufferedReader br = new BufferedReader(new FileReader("./data/bogota_vertices.txt"));
			String linea = br.readLine();
			String[] info = null; 
			while((linea = br.readLine()) != null)
			{
				info = linea.split(";");
				grafo.addVertex(Integer.parseInt(info[0]), new Interseccion(Integer.parseInt(info[0]), Double.parseDouble(info[1]), Double.parseDouble(info[2]), Integer.parseInt(info[3])));
			}
			br.close();

			br = new BufferedReader(new FileReader("./data/bogota_arcos.txt"));
			linea = "";
			info = null;

			while((linea = br.readLine()) != null)
			{
				info = linea.split(" ");
				int interseccion = Integer.parseInt(info[0]);
				for(int i = 1; i < info.length;i++)
				{
					Interseccion ini = grafo.getInfoVertex(interseccion);
					Interseccion fin = grafo.getInfoVertex(Integer.parseInt(info[i]));
					if(fin != null && ini != null)
						grafo.addEdge(ini.id, fin.id, haversine(ini.latitud, ini.longitud, fin.latitud, fin.longitud));
				}
			}
			br.close();	


		} 
		catch (FileNotFoundException e) 
		{
			e.printStackTrace();
		}

	}

	/**
	 * Método que da el número de componentes conectadas del grafo
	 * @return Número de componentes conectadas. cc >= 0
	 */
	public int cc()
	{
		cc = grafo.cc();
		return cc;
	}
	
	public void mapa()
	{
		mapa = new Maps(grafo);
	}

	//---------------------------------------
	// Escribir y cargar JSON 
	//---------------------------------------

	/**
	 * Método que escribe un archivo JSON del grafo
	 */
	@SuppressWarnings("unchecked")
	public void crearJSON()
	{	
		JSONObject vertice = new JSONObject();
		JSONArray vertices = new JSONArray();
		JSONObject arco = new JSONObject();
		
		int id = -1;

		Iterator<Integer> llaves = grafo.vertices.iterator();
		//Recorre todos los vertices
		while(llaves.hasNext())
		{
			//Arcos asociados a el vertice del momento
			JSONArray arcos = new JSONArray();
			
			id = llaves.next();

			//Vertice del momento
			Interseccion inter = grafo.getInfoVertex(id);

			//Agrega los valores del vertice al objeto JSON
			vertice.put("id", inter.id);
			vertice.put("log", inter.longitud);
			vertice.put("lat", inter.latitud);
			vertice.put("MOVEMENT_ID", inter.movement_id);
			
			//Nodo que recorre todos los vertices conectados a inter
			Nodo<Arco<Integer>> tempo = grafo.adj[inter.id].primero;
			
			//Ciclo que recorre los vertices 
			while(tempo != null)
			{
				//Añade al arreglo el id del vertice destino
				arco.put("id", tempo.value.finVertice);
				arco.put("costo", tempo.value.costo);
				arcos.add(arco);
				tempo = tempo.sig;
				arco = new JSONObject();
			}

			//Agrega los arcos asociados al vertice al objeto JSON
			vertice.put("arcos", arcos);
			
			vertices.add(vertice);
			
			vertice = new JSONObject();
		}

		//Objeto JSON con el número de vertices
		JSONObject numVertices = new JSONObject();
		numVertices.put("#Vertices", grafo.V);
		
		//Objeto JSON con el número de arcos
		JSONObject numArcos = new JSONObject();
		numArcos.put("#Arcos", grafo.E);
		
		//Objeto JSON con el número de componentes conectadas
		JSONObject numCC = new JSONObject();
		numCC.put("#CC", cc);
		
		//Arreglo con toda la información del grafo
		JSONArray info_grafo = new JSONArray();
		info_grafo.add(numVertices);
		info_grafo.add(numArcos);
		info_grafo.add(numCC);

		//Objeto JSON que tiene la información del grafo y la información de cada vertice
		JSONObject objeto = new JSONObject();
		objeto.put("info grafo", info_grafo);
		objeto.put("vertices", vertices);


		try (FileWriter file = new FileWriter("./docs/vertices.json")) {

			file.write(objeto.toString());
			file.flush();

		} 
		catch (IOException e) 
		{
			e.printStackTrace();
		}
	}

	/**
	 * Método que carga el archivo JSON del grafo
	 * @throws IOException 
	 */
	public GrafoNoDirigido<Integer, Interseccion> cargarJSON() throws IOException
	{
		GrafoNoDirigido<Integer, Interseccion> grafoJSON = null;
		long V = 0;
		
		JSONParser parser = new JSONParser();
		FileReader reader = null;
		try
		{
			reader = new FileReader("./docs/vertices.json");
			Object obj = parser.parse(reader);
			JSONObject jsonObject = (JSONObject) obj;
			
			JSONArray info_grafo = (JSONArray)jsonObject.get("info grafo");
			JSONObject numVertices = (JSONObject) info_grafo.get(0);
			V = (long) numVertices.get("#Vertices");			
			grafoJSON = new GrafoNoDirigido<Integer, Interseccion>((int) V);
		
			JSONArray vertices = (JSONArray) jsonObject.get("vertices");
			for(int i = 0; i < vertices.size(); i++)
			{
				JSONObject vertice = (JSONObject) vertices.get(i);
				long id = (long) vertice.get("id");
				double log = (double) vertice.get("log");
				double lat = (double) vertice.get("lat");
				long MOVEMENT_ID = (long) vertice.get("MOVEMENT_ID");
				grafoJSON.addVertex((int)id, new Interseccion((int)id,log,lat,(int)MOVEMENT_ID));
				
				JSONArray arcos = (JSONArray) vertice.get("arcos");
				for(int j = 0; j < arcos.size(); j++)
				{
					JSONObject arco = (JSONObject) arcos.get(j);
					long id_arco = (long) arco.get("id");
					double costo = (double) arco.get("costo");
					if(grafoJSON.getCostArc((int) id, (int) id_arco) == -1)
						grafoJSON.addEdge((int) id, (int) id_arco, costo);
				}
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			reader.close();
		}
		
		return grafoJSON;
	}
	//---------------------------------------
	// Formula Haversine 
	//---------------------------------------
	/**
	 * Método de la formula Haversine
	 * Método sacado de: https://www.geeksforgeeks.org/haversine-formula-to-find-distance-between-two-points-on-a-sphere/
	 * @param lat1 Latitud de la primera coordenada. 
	 * @param lon1 Longitud de la segunda coordenada.
	 * @param lat2 Latitud de la primera coordenada.
	 * @param lon2 Longitud de la segunda coordenada.
	 * @return
	 */
	private double haversine(double lat1, double lon1, double lat2, double lon2) 
	{ 
		// distance between latitudes and longitudes 
		double dLat = Math.toRadians(lat2 - lat1); 
		double dLon = Math.toRadians(lon2 - lon1); 

		// convert to radians 
		lat1 = Math.toRadians(lat1); 
		lat2 = Math.toRadians(lat2); 

		// apply formulae 
		double a = Math.pow(Math.sin(dLat / 2), 2) +  
				Math.pow(Math.sin(dLon / 2), 2) *  
				Math.cos(lat1) *  
				Math.cos(lat2); 
		double rad = 6371; 
		double c = 2 * Math.asin(Math.sqrt(a)); 
		return rad * c; 
	} 

}
