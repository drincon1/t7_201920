package test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import model.data_structures.Arco;
import model.data_structures.GrafoNoDirigido;
import model.data_structures.Nodo;


public class testGrafoNoDirigido 
{
	GrafoNoDirigido<Integer, String> grafo;
	@Before
	public void setUp()
	{
		grafo = new GrafoNoDirigido<Integer, String>(13);
		grafo.addVertex(1, "Uno");
		grafo.addVertex(2, "Dos");
		grafo.addVertex(3, "Tres");
		
		grafo.addVertex(11, "a");
		grafo.addVertex(12, "b");
		
		grafo.addEdge(1, 2, 10.9);
		grafo.addEdge(1, 3, 5);
		grafo.addEdge(11, 12, 0.92);
	}
	
	@Test
	public void testGrafo()
	{
		assertEquals("Uno", grafo.getInfoVertex(1));
		assertEquals(null, grafo.getInfoVertex(0));
		
		grafo.setInfoVertex(1, "Once");
		assertEquals("Once", grafo.getInfoVertex(1));
		
		assertTrue(10.9 == grafo.getCostArc(1, 2));
		grafo.setCostArc(1, 2, 11);
		assertTrue(11 == grafo.getCostArc(1, 2));
			
		
		
	}
}
