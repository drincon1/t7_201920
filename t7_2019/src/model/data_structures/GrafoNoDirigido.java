package model.data_structures;


//MinHeap lo mejor para camino más corto

public class GrafoNoDirigido<K extends Comparable<K>,V>  implements IGrafoNoDirigido<K,V>
{
	//---------------------------------------------
	// Atributos
	//---------------------------------------------
	/**
	 * Número de vertices
	 */
	public int V = 0;

	/**
	 * Número de arcos
	 */

	public int E;


	/**
	 * Un arreglo de listas encadenadas
	 * En cada posición estan los arcos del vertice
	 */
	public ListaEncadenada<Arco<K>>[] adj;

	/**
	 * Tabla Hash Linear Proving con los vertices
	 */
	public HashTable<K,Vertice<K,V>> vertices;

	/**
	 * Vertices marcados
	 */
	public boolean[] marked;

	/**
	 * Número de nodos contados
	 */
	public int count;

	/**
	 * 
	 */
	public int[] id;

	//---------------------------------------------
	// Constructor 
	//---------------------------------------------
	/**
	 * Constructor de un grafo no dirigido
	 * El grafo se crea sin arcos
	 * @param V Número de vértices
	 */
	public GrafoNoDirigido(int V) 
	{
		//Número de vertices
		this.V = V;
		
		//Agrega una lista encadenada a cada posicion del arreglo adj
		adj = new ListaEncadenada[V+1];
		for(int v = 0; v < adj.length; v++)
			adj[v] = new ListaEncadenada<Arco<K>>();

		//Tabla de hash de los vertices
		vertices = new HashTable<K, Vertice<K,V>>(V);

		//El arreglo marked se crea con V posiciones porque puede que todos los vertices queden marcados
		marked = new boolean[V+1];

		//Cuando se crea el grafo no hay vertices marcados
		count = 0;

		id = new int[V+1];		
	}

	//---------------------------------------------
	// Funcionamiento 
	//---------------------------------------------

	@Override
	public int V() 
	{
		return V;
	}

	@Override
	public int E() 
	{
		return E;
	}

	@Override
	public void addEdge(K idVertexIni, K idVertexFin, double cost) 
	{			
		int ini = (Integer) idVertexIni;
		int fin = (Integer) idVertexFin;
		
		adj[ini].add(new Arco<K>(idVertexIni, idVertexFin, cost));
		adj[fin].add(new Arco<K>(idVertexFin, idVertexIni, cost));

		E++;
	}

	@Override
	public V getInfoVertex(K idVertex) 
	{
		if(vertices.get(idVertex) != null)
			return vertices.get(idVertex).valor;
		return null;
	}

	@Override
	public void setInfoVertex(K idVertex, V infoVertex) 
	{
		vertices.put(idVertex, new Vertice<K,V>(idVertex, infoVertex));
	}

	@Override
	public double getCostArc(K idVertexIni, K idVertexFin) 
	{
		Nodo<Arco<K>> tempo = adj[(Integer) idVertexIni].primero;
		while(tempo != null)
		{
			if(tempo.value.finVertice.equals(idVertexFin))
				return tempo.value.costo;
			tempo = tempo.sig;
		}

		return -1;
	}

	@Override
	public void setCostArc(K idVertexIni, K idVertexFin, double cost) 
	{
		Nodo<Arco<K>> tempo = adj[(Integer) idVertexIni].primero;
		while(tempo != null)
		{
			if(tempo.value.finVertice.equals(idVertexFin))
			{
				tempo.value.costo = cost;
				break;
			}
			tempo = tempo.sig;
		}
	}

	@Override
	public void addVertex(K idVertex, V infoVertex) 
	{
		Vertice<K,V> vertice = new Vertice<K,V>(idVertex, infoVertex);
		vertices.put(idVertex, vertice);
	}

	@Override
	public Iterable<K> adj(K idVertex) 
	{
		return null;
	}

	@Override
	public void uncheck() 
	{
		marked = new boolean[V+1];
		count = 0;

	}

	@Override
	public void dfs(K s)
	{
		marked[(Integer) s] = true;
		id[(Integer) s] = count;
		Nodo<Arco<K>> tempo = adj[(Integer) s].primero;
		while(tempo != null)
		{
			if(!marked[(Integer) tempo.value.finVertice])
				dfs(tempo.value.finVertice);
			tempo = tempo.sig;
		}

	}

	@Override
	public int cc() 
	{
		for(int s = 0; s < V();s++)
		{
			K tempo = vertices.getKeys()[s];
			if(tempo != null)
			{
				if(!marked[(Integer) tempo ])
				{
					dfs(tempo);
					count++;
				}
			}
		}
		return count;
	}

	@Override
	public Iterable<K> getCC(K idVertex) 
	{
		// TODO Auto-generated method stub
		return null;
	}









}
