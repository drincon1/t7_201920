package model.data_structures;

public class Arco<K>
{
	//---------------------------------------------
	// Atributos
	//---------------------------------------------
	/**
	 * El vertice inicial del arco
	 */
	public K iniVertice;
	
	/**
	 * El vertice final del arco
	 */
	public K finVertice;
	
	/**
	 * El costo del arco
	 */
	public double costo;
	
	//---------------------------------------------
	// Constructor 
	//---------------------------------------------
	
	public Arco(K iniVertice, K finVertice, double costo)
	{
		this.iniVertice = iniVertice;
		this.finVertice = finVertice;
		this.costo = costo;
	}
	
}
