package main;

import java.io.IOException;

import controller.Controller;

public class MVC {

	//---------------------------------------------
	// Atributos
	//---------------------------------------------
	private static Controller controller; 

	//---------------------------------------------
	// Constructor 
	//---------------------------------------------
	public static void main(String[] args) throws IOException 
	{
		controller = new Controller();
		controller.run();

	}




}
