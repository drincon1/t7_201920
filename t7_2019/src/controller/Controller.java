package controller;

import java.io.IOException;

import model.logic.MVCModelo;
import view.MVCView;

public class Controller 
{
	//---------------------------------------------
	// Atributos
	//---------------------------------------------

	//Referencia al modelo
	private MVCModelo modelo;
	//Referencia al view
	private MVCView view;

	//---------------------------------------------
	// Constructor 
	//---------------------------------------------
	/**
	 * Método constructor de la clase Controller
	 * Inicializa las variables modelo y view
	 */
	public Controller()
	{
		modelo = new MVCModelo();
		view  = new MVCView();
	}

	//---------------------------------------------
	// Métodos
	//---------------------------------------------
	public void run() throws IOException
	{
		modelo.cargar();
		view.printCarga();
		view.printCC(modelo.cc());
		modelo.crearJSON();
		view.printGrafoJSON(modelo.cargarJSON());
	}
	
}
