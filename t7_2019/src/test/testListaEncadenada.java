package test;

import org.junit.Before;
import org.junit.Test;

import model.data_structures.ListaEncadenada;
import model.data_structures.Nodo;

public class testListaEncadenada 
{
	ListaEncadenada<Integer> lista;
	@Before
	public void setUp()
	{
		lista= new ListaEncadenada<Integer>();
		for(int i = 1; i < 11; i++)
			lista.add(i);
		lista.add(1);
	
	}
	
	@Test
	public void testLista()
	{
		Nodo<Integer> tempo = lista.primero;
		while(tempo != null)
		{
			System.out.println(tempo.value);
			tempo = tempo.sig;
		}
	}
}
