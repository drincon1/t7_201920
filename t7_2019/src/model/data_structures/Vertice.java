package model.data_structures;

public class Vertice<K,V> 
{
	//---------------------------------------------
	// Atributos
	//---------------------------------------------
	public boolean marcado;
	
	public K identificacion;
	
	public V valor;
	//---------------------------------------------
	// Constructor 
	//---------------------------------------------
	
	public Vertice(K identificacion, V valor)
	{
		marcado = false;
		this.identificacion = identificacion;
		this.valor = valor;
	}
	//---------------------------------------------
	// Métodos 
	//---------------------------------------------
	
}
